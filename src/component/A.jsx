import React, { useState } from "react";
import B from "./B";

const A = () => {
  let [count, setCount] = useState(1);
  return (
    <div>
      A<B count={count}></B>
    </div>
  );
};

export default A;
